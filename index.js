const FIRST_NAME = "Tudor";
const LAST_NAME = "Stefan";
const GRUPA = "1085";

function initCaching() {

    let cache = new Object();

    return {
        pageAccessCounter: function (page) {
            if (typeof (page) === 'undefined')
            {
                page = 'home';
            }
            page = page.toLowerCase();

            if (page in cache) {
                cache[page]++;
            } else {
                cache[page] = 1;
            }
        },
        getCache: function () {
            return cache;
        }
    };
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

